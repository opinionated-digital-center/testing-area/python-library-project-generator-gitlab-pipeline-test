#!/usr/bin/env python
"""Tests for `python_library_project_generator_gitlab_pipeline_test` package."""
from python_library_project_generator_gitlab_pipeline_test import core


def test_hello_world():
    """Test hello world."""
    assert core.hello_world() == "Hello world!"
