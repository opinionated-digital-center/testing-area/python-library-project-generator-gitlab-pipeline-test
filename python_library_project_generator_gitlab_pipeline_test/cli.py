"""Console script for python_library_project_generator_gitlab_pipeline_test."""

import sys

import cleo

from . import __version__
from .core import hello_world


class HelloCommand(cleo.Command):
    """
    First Command

    hello
    """

    def handle(self):
        self.line(hello_world())


class Application(cleo.Application):
    def __init__(self):
        super().__init__("Generated Python Project GitLab", __version__)

        self.add(HelloCommand())


def main(args=None):
    return Application().run()


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
