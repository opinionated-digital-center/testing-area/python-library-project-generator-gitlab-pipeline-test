"""All package specific exceptions."""


class PythonLibraryProjectGeneratorGitlabPipelineTestError(Exception):
    """
    Base exception for errors raised by
    python_library_project_generator_gitlab_pipeline_test.
    """
