==============================================
Introdution to Generated Python Project GitLab
==============================================

.. image:: https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/badges/master/pipeline.svg
    :target: https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/pipelines

.. image:: https://readthedocs.org/projects/python-library-project-generator-gitlab-pipeline-test/badge/?version=latest
        :target: https://python-library-project-generator-gitlab-pipeline-test.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

Project `published on TestPyPi <https://test.pypi.org/project/python-library-project-generator-gitlab-pipeline-test>`_.

A Generated Python Project contains all the boilerplate you need to create a Python package.

* Free software license: MIT
* Documentation: https://python-library-project-generator-gitlab-pipeline-test.readthedocs.io

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the
`opinionated-digital-center/python-library-project-generator`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`opinionated-digital-center/python-library-project-generator`: https://github.com/opinionated-digital-center/python-library-project-generator
