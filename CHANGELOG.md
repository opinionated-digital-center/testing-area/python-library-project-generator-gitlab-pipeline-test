# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.38.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.37.0...v0.38.0) (2021-01-14)


### Features

* 21-01-14_22h21-33_607903 ([73f9e20](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/73f9e209513ba2ca653f2861f9db1f4789ec1946))

## [0.37.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.36.0...v0.37.0) (2021-01-14)


### Features

* 21-01-14_20h56-30_484126 ([9123414](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/91234146b8d3ada8ce7f0c4ae42309329ed2e9c1))

## [0.36.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.35.0...v0.36.0) (2020-05-15)


### Features

* 20-05-15_21h51-45_051824 ([5501e60](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/5501e60f70d42d8fceac7dbd5893e75e162d2d74))

## [0.35.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.34.0...v0.35.0) (2020-05-13)


### Features

* 20-05-13_14h53-13_630326 ([7658610](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/765861068c59a02827e6ffcf785ba7eddd9b7af1))

## [0.34.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.33.0...v0.34.0) (2020-05-06)


### Features

* 20-05-06_09h09-09_166988 ([dc130e6](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/dc130e6d7e6dfe395c81852bd2973ef7e50ee703))

## [0.33.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.32.0...v0.33.0) (2020-05-05)


### Features

* 20-05-05_11h15-59_132712 ([97c093d](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/97c093d230f82effab80558da64504fd7ac97ad0))

## [0.32.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.31.0...v0.32.0) (2020-05-04)


### Features

* 20-05-04_12h22-44_170505 ([ae837db](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/ae837db7be8287652a891ff8d99e6d1ab24d4849))

## [0.31.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.30.0...v0.31.0) (2020-05-04)


### Features

* 20-05-04_09h59-16_771731 ([f7d725d](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/f7d725dfab6450304719b4c4f20c4a67c61d019a))

## [0.30.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.29.0...v0.30.0) (2020-04-30)


### Features

* 20-04-30_23h10-06_286975 ([dc44271](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/dc442716b230a905c9838a90919d5f21d15e3b4e))

## [0.29.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.28.0...v0.29.0) (2020-04-30)


### Features

* 20-04-30_21h40-04_534652 ([87ec46d](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/87ec46d17b82722cce86e190dbea652cbfa3debf))

## [0.28.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.27.0...v0.28.0) (2020-04-30)


### Features

* 20-04-30_20h01-37_357473 ([1fd0a9c](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/1fd0a9ce37252b2728def8919be34de15bb1b910))

## [0.27.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.26.0...v0.27.0) (2020-04-29)


### Features

* 20-04-29_19h36-28_721561 ([6957f24](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/6957f24dc630f5118825c3d286ba067c16eed9ff))

## [0.26.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.25.1...v0.26.0) (2020-04-29)


### Features

* 20-04-29_19h15-23_444261 ([477492e](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/477492eaec908b52b829f5e646992aed0466a24f))

## [0.25.1](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.25.0...v0.25.1) (2020-04-29)


### Bug Fixes

* correct CHANGELOG.md ([04dbbef](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/04dbbefd51b6902d6933e82fbd0fbf74e3a66202))

## [0.25.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.24.0...v0.25.0) (2020-04-29)


### Features

* 20-04-29_20h49-59_065209 ([1e4b6a8](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/1e4b6a8110aa9a5ceefeffd315d9d99724f05fd5))

## [0.24.0](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/compare/v0.23.0...v0.24.0) (2020-04-27)


### Features

* 20-04-27_13h54-38_306280 ([364b96a](https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test/commit/364b96aca11488188534845697e18175411b09cf))
