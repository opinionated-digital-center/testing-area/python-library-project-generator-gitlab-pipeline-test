.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Generated Python Project GitLab, run this command in your terminal:

.. code-block:: console

    $ pip install python-library-project-generator-gitlab-pipeline-test

This is the preferred method to install Generated Python Project GitLab, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Generated Python Project GitLab can be downloaded
from the `GitLab repo`_.

Clone the public repository:

.. code-block:: console

    $ git clone https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test.git

Then install it with:

.. code-block:: console

    $ cd python-library-project-generator-gitlab-pipeline-test
    $ POETRY_VIRTUALENVS_CREATE=FALSE poetry install


.. _GitLab repo: https://gitlab.com/opinionated-digital-center/testing-area/python-library-project-generator-gitlab-pipeline-test
